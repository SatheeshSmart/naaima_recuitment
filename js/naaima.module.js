(function(){
angular.module('naaimarecuitment',['naaima.router','naaima.controller','naaima.service'])
		.run(['$window','$rootScope','authenticationService','$state',
           function(window,globalScope,authenticationService,state){
			globalScope.showProgress = false;
			globalScope.$on('$stateChangeStart', function(e,toState,toParams,fromState,fromParams){
                var isLogin = toState.name === "login";
                if(isLogin && !authenticationService.isUserAuthorised()){
                	return; // no need to redirect 
                }
                // now, redirect only not authenticated
                if(authenticationService.isUserAuthorised() === false) {
                    e.preventDefault(); // stop current execution
                    state.go('login'); // go to login
                }
            });
		}])
		.directive('naaimaHeader',function(){
			return {
				restrict:'AE',
				templateUrl:'partials/header.html',
				controller:'HeaderController'
			}
		})
		.directive('naaimaFooter',function(){
			return {
				restrict:'AE',
				templateUrl:'partials/footer.html'
			}
		});


})();
