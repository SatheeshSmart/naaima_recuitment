(function() {
  angular.module('naaima.router', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/addresource');

      $stateProvider
      // Login ========================================
        .state('login', {
          url: '/login',
          templateUrl: 'partials/login.html',
          controller:'LoginController'
        })
        .state('resourcelist', {
          url: '/resourcelist',
          templateUrl: 'partials/resourcelist.html',
          controller:'GetResourceController'
        })
        .state('addresource', {
          url: '/addresource',
          templateUrl: 'partials/addresource.html',
          controller: 'AddEmployeeController'
        });
    }]);
})();
