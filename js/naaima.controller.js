(function() {
  angular.module('naaima.controller', ['ui.bootstrap'])
  .controller('HeaderController',['$scope','$rootScope','$window',function(model,globalScope,window){
	  var userSession = window.sessionStorage.getItem('userInfo');
	  if(userSession){
		  globalScope.userDetails = JSON.parse(userSession);
	  }
  }])
  .controller('LoginController',['$scope','$rootScope','$window','$state','authenticationService',
    function(model,globalScope,window,state,authenticationService){
	  globalScope.userDetails = {};
      model.user = {
    		  userName:'',
    		  password:''
      }
      model.userValid = true;
      model.login = function(user){
    	  globalScope.showProgress = true;
    	  authenticationService.authenticateUser(user).then(function(data){
    		  if(data.Role == 'User'){
    			    globalScope.userDetails['Name'] = data.User_Name;
        		  globalScope.userDetails['Recuiter_ID'] = data.Recuiter_ID;
              globalScope.userDetails['Role'] = data.Role;
        		  window.sessionStorage["userInfo"] = JSON.stringify(globalScope.userDetails);
        		  globalScope.showProgress = false;
        		  state.go('addresource');
    		  }else{
    			  model.userValid = false;
    			  globalScope.showProgress = false;
    		  }
    	  })
      }
  }])
  .controller('AddEmployeeController', ['$scope','$rootScope','recuitmentservice','notificationService',
      function(model,globalScope,recuitmentservice,notifications) {
      model.resource = {
    		  Date:'',
    			Client:'',
    			Position_Type:'',
    			Requirement_ID:'',
    			Name:'',
    			Contact_No:'',
    			Email_ID:'',
    			Skill:'',
    			Pancard_No:'',
    			Skype_ID:'',
    			Total_Exp:'',
    			Relevant_Exp:'',
    			Current_CTC:'',
    			Expected_CTC:'',
    			Notice_Period:'',
    			Current_Company:'',
    			Type_of_Employment:'',
    			Current_Location:'',
    			Preferred_Location:'',
    			Status:'',
    			Remarks:'',
          Recuiter:''
    		}
      var resetForm = function(){
        model.resource = {};
        model.resourceForm.$setPristine();
        model.resourceForm.$setValidity();
      }
      model.addResource = function(isFormValid) {
    	  globalScope.showProgress = true;
    	  if(!isFormValid){
    		  console.log('Invalid Form');
          globalScope.showProgress = false;
          notifications.naaimaWarning('Please enter the required feilds');
    	  }else{
          model.resource.Recuiter = globalScope.userDetails['Recuiter_ID'];
    		  recuitmentservice.addResource(model.resource).then(function(data) {
    			  globalScope.showProgress = false;
    			  if(data.status == 'Success'){
                notifications.naaimaSuccess('Resource Added Successfully');
                resetForm();
            }else{
                notifications.naaimaWarning('Internal Server Error');
            }
    	        })
    	  }

      }
    }])
    .controller('GetResourceController',['$scope','$rootScope','recuitmentservice', function(model, globalScope,recuitmentservice){
    	model.resources = [];
      model.itemsPerPage = 13;
      model.currentPage = 1;
    	getResources();
    	function getResources(){
    		globalScope.showProgress = true;
    		return recuitmentservice.getResources().then(function(data){
    			globalScope.showProgress = false;
          model.resources = data;
          model.totalItems = data.length;
          model.$watch('currentPage + itemsPerPage', function() {
            var begin = ((model.currentPage - 1) * model.itemsPerPage),
              end = begin + model.itemsPerPage;

            model.filteredresources = model.resources.slice(begin, end);
          });

    		})
    	}
    }])
})();
