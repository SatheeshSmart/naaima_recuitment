(function() {
  angular.module('naaima.service', ['toastr'])
    .factory('notificationService',['toastr',function(toastr){
      var services = {
        naaimaSuccess:naaimaSuccess,
        naaimaInfo:naaimaInfo,
        naaimaError:naaimaError,
        naaimaWarning:naaimaWarning
      }
      return services;
      function naaimaSuccess(message){
          toastr.success(message);
      }
      function naaimaInfo(message){
          toastr.info(message);
      }
      function naaimaError(message){
          toastr.error(message);
      }
      function naaimaWarning(message){
          toastr.warning(message);
      }
    }])
    .factory('recuitmentservice', ['$http', function(http) {
      var dataservices = {
        addResource: addResource,
        getResources: getResources
      }
      return dataservices;

      function addResource(employee) {
        var request = {
          'method': 'POST',
          'url': 'services/insertResource',
          data: employee,
          'Content-Type':'application/json',
          'Accept':'application/json'
        }
        return http(request)
          .then(returnEmployees)
          .catch(function(message) {
            console.log('XHR failed while Adding Resource');
          });

        function returnEmployees(data, status, headers, config) {
          return data.data;
        };
      }

      function getResources() {
        return http.get('services/resources')
          .then(returnEmployees)
          .catch(function(message) {
            console.log('XHR failed while Getting Resources');
          });

        function returnEmployees(data, status, headers, config) {
          return data.data;
        };
      }
    }])
    .factory('authenticationService',['$http','$window',function(http,window){
    	var service = {
    			authenticateUser:authenticateUser,
    			isUserAuthorised:isUserAuthorised
    	}
    	return service;

    	function authenticateUser(user){
    		var request = {
    				'method': 'POST',
    		          'url': 'services/login',
    		          data: user,
    		          'Content-Type':'application/json',
    		          'Accept':'application/json'
    		}
    		return http(request)
    				.then(returnResult)
    				.catch(function(message){
    					console.log('XHR failed while Authenticating User');
    				});
    		function returnResult(data,status,headers,config){
    			return data.data;
    		}
    	};

    	function isUserAuthorised(){
    		var isAuthorised = false;
    		var userDetails = window.sessionStorage.getItem('userInfo');
    		userDetails = JSON.parse(userDetails);
        if(userDetails){
    			isAuthorised = true;
    		}
    		return isAuthorised;
    	};
    }]);

})();
